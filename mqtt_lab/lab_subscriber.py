import paho.mqtt.client as mqtt
import time
import json

def on_connect(client, userdata, flags, return_code):
    if return_code == 0:
        print("connected")
        client.subscribe("Motion_Detected")
    else:
        print("could not connect, return code:", return_code)


def on_message(client, userdata, message):
	payload_message = message.payload.decode("utf-8")
	message_json = json.loads(payload_message)

	print("Received message")
	text = message_json['timestamp'] + " new event detected in __Motion_Detected__:movement in room:" + message_json['picture']
	print(text)
	the_file = open("file.txt", "a")
	the_file.writelines(text + "\n")
	the_file.close()


broker_hostname ="localhost"
port = 1883 

client = mqtt.Client("Client2")
client.username_pw_set(username="user1", password="password1")
client.on_connect=on_connect
client.on_message=on_message

client.connect(broker_hostname, port) 
client.loop_start()

try:
    time.sleep(10)
finally:
    client.loop_stop()
