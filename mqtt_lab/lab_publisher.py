import time
import signal
import datetime
import json
import paho.mqtt.client as mqtt
import sys

the_motion = False
the_timestamp = ""
message = ''
url = "url_new_picture"

broker_hostname = "localhost"
port = 1883 

def on_connect(client, userdata, flags, return_code):
    print("CONNACK received with code %s." % return_code)
    if return_code == 0:
        print("connected")
    else:
        print("could not connect, return code:", return_code)

client = mqtt.Client(client_id="Client1", userdata=None)
client.on_connect = on_connect

client.username_pw_set(username="user1", password="password1")


client.connect(broker_hostname, port, 60)
#client.loop_forever()
client.loop_start()


try:
	while(True):
		time.sleep(2)
		if the_motion == True:
			the_timestamp = 'Traffic offence recorded in %s' % str(datetime.datetime.now())
			print(the_timestamp)
			message = json.dumps({'motion':the_motion, 'timestamp':the_timestamp, 'picture':url})
			topic = "Motion_Detected"
			
			result = client.publish(topic=topic, payload=message)
			status = result[0]
			if status == 0:
				print("Message is published to topic " + topic)
			the_motion = False
		elif the_motion == False:
			the_motion = True
finally:
    client.loop_stop()
    sys.exit()
