'''
Example from https://github.com/grpc/grpc/tree/master/examples/python
'''

from __future__ import print_function

import logging

import grpc
import helloworld_pb2
import helloworld_pb2_grpc

#python -m grpc_tools.protoc -I./protos --python_out=. --pyi_out=. --grpc_python_out=. ./protos/helloworld.proto

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    print("Will try to greet world ...")
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = helloworld_pb2_grpc.GreeterStub(channel)
        response1 = stub.SayHello(helloworld_pb2.HelloRequest1(name="Simona",week=100, average=80))
        response2 = stub.SayGoodBye(helloworld_pb2.HelloRequest2(first_name="Simona",last_name="Dragomir"))
    print("Greeter client received: " + response1.message)
    print("Greeter client received: " + response2.message)


if __name__ == "__main__":
    logging.basicConfig()
    run()
