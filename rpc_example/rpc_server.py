'''
Example from https://github.com/grpc/grpc/tree/master/examples/python
'''

from concurrent import futures
import logging
import time

import grpc
import helloworld_pb2
import helloworld_pb2_grpc


class Greeter(helloworld_pb2_grpc.GreeterServicer):
    def SayHello(self, request, context):
        print(f'Received from client: {request.name}, {request.average} and {request.week}. Now... Processing your message...')
        time.sleep(5)
        print('Processing finished, now will return to client')
        return helloworld_pb2.HelloReply1(message=f"Hello, {request.name}! RPC example - week {request.week} - average {request.average}.")
    def SayGoodBye(self, request, context):
        print(f'Received from client: {request.first_name} and {request.last_name}. Now... Processing your message...')
        time.sleep(5)
        print('Processing finished, now will return to client')
        return helloworld_pb2.HelloReply2(message=f"Good Bye {request.first_name} {request.last_name}!")


def serve():
    port = "50051"
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    helloworld_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    server.add_insecure_port("[::]:" + port)
    server.start()
    print("Server started, listening on " + port)
    server.wait_for_termination()


if __name__ == "__main__":
    logging.basicConfig()
    serve()
