import requests
import json

response_json = None;

def make_request():
    url = "http://10.172.25.216:5080/WeatherForecast"
    response = requests.get(url)
    response_json = response.json()
    print(response.json())
    return response_json

response_json = make_request()

'''
Dashboard
Example from 
https://dash.plotly.com/dash-daq
https://dash.plotly.com/dash-core-components/interval
'''
from dash import Dash, html, dcc, Input, Output, callback
import dash_daq as daq

app = Dash(__name__)

app.layout = html.Div([
    daq.Thermometer(
        id='my-thermometer-1',
        label=response_json[0]["date"] + " " +response_json[0]["summary"],
        value=response_json[0]["temperatureC"],
        min=-60,
        max=60,
        style={
            'margin-bottom': '5%',
            'margin-left': '5%',
            'display': 'inline-block'
        }
    ),
    daq.Thermometer(
        id='my-thermometer-2',
        label=response_json[1]["date"] + " " +response_json[1]["summary"],
        value=response_json[1]["temperatureC"],
        min=-60,
        max=60,
        style={
            'margin-bottom': '5%',
            'margin-left': '10%',
            'display': 'inline-block'
        }
    ),
    daq.Thermometer(
        id='my-thermometer-3',
        label=response_json[2]["date"] + " " +response_json[2]["summary"],
        value=response_json[2]["temperatureC"],
        min=-60,
        max=60,
        style={
            'margin-bottom': '5%',
            'margin-left': '10%',
            'display': 'inline-block'
        }
    ),
    daq.Thermometer(
        id='my-thermometer-4',
        label=response_json[3]["date"] + " " +response_json[3]["summary"],
        value=response_json[3]["temperatureC"],
        min=-60,
        max=60,
        style={
            'margin-bottom': '5%',
            'margin-left': '10%',
            'display': 'inline-block'
        }
    ),
    daq.Thermometer(
        id='my-thermometer-5',
        label=response_json[4]["date"] + " " +response_json[4]["summary"],
        value=response_json[4]["temperatureC"],
        min=-60,
        max=60,
        style={
            'margin-bottom': '5%',
            'margin-left': '10%',
            'display': 'inline-block'
        }
    ),
    dcc.Interval(
        id='interval-component',
        interval=5*1000, # in milliseconds
        n_intervals=0
    ),
])

value=1
@callback(
    Output('my-thermometer-1', 'value'),
    Output('my-thermometer-1', 'label'),
    Output('my-thermometer-2', 'value'),
    Output('my-thermometer-2', 'label'),
    Output('my-thermometer-3', 'value'),
    Output('my-thermometer-3', 'label'),
    Output('my-thermometer-4', 'value'),
    Output('my-thermometer-4', 'label'),
    Output('my-thermometer-5', 'value'),
    Output('my-thermometer-5', 'label'),
    Input('interval-component', 'n_intervals')
)
def update_thermometer(value):
	value=make_request()
	lable_1 = value[0]["date"] + " " +value[0]["summary"]
	lable_2 = value[1]["date"] + " " +value[1]["summary"]
	lable_3 = value[2]["date"] + " " +value[2]["summary"]
	lable_4 = value[3]["date"] + " " +value[3]["summary"]
	lable_5 = value[4]["date"] + " " +value[4]["summary"]
	
	return (value[0]["temperatureC"],lable_1,value[1]["temperatureC"],lable_2,value[2]["temperatureC"],lable_3,value[3]["temperatureC"],lable_4,value[4]["temperatureC"],lable_5)


if __name__ == '__main__':
    app.run(debug=True,host = '10.172.25.216')
