'''
Example from https://www.digitalocean.com/community/tutorials/python-socket-programming-server-client
'''
import socket


def client_program():
    host = socket.gethostname()  # as both code is running on same pc
    #If you connect from a different host, change to your IP (or hostname if you are using DNS)
    #host = '192.168.0.132'
    port = 15789  # socket server port number

    client_socket = socket.socket()  # instantiate
    client_socket.connect((host, port))  # connect to the server
    
    print('Connected to host '+str(host)+' in port: '+str(port))
    message = client_socket.recv(1024)
    print("Message received from the server", message.decode("utf-8"))
    
    message = input(" -> ")  # take input

    while message.lower().strip() != 'quit':
        client_socket.send(message.encode("utf-8"))  # send message
        data = client_socket.recv(1024).decode("utf-8")  # receive response

        print('Received from server: ' + data)  # show in terminal

        message = input(" -> ")  # again take input

    client_socket.close()  # close the connection


if __name__ == '__main__':
    client_program()